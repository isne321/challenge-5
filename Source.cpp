#include<iostream>
#include<string>
#include <cctype> //toupper

using namespace std;

void palindromeCheck(string&);

int main() {
	string phase;
	palindromeCheck(phase);
	return 0;
}

void palindromeCheck(string& phase) {
	getline(cin, phase);
	string phase_temp = phase; //make a copy to calculate
	bool palindrome = true;

	for (int i = 0; i < phase_temp.length(); i++) { //Converts characters to uppercase
		phase_temp[i] = toupper(phase_temp[i]);
	}

	for (int i = 0; i < phase_temp.length(); i++) { //Remove spacial symbol except uppercase
		if (phase_temp[i] < 65 || phase_temp[i] > 90) { //ASCII
			phase_temp.erase(i, 1);
			i--;
		}
	}
	//cout << phase_temp << endl;

	
	for (int i = 0, j = phase_temp.length() - 1; i <= (phase_temp.length() / 2); i++, j--) { //Check first and last character in pair
		if (phase_temp[i] != phase_temp[j]) {
			palindrome = false;
		}
	}

	if (palindrome == true) {
		cout << "The word  '" << phase << "' is Palindrome!"<<endl;
	}
	else {
		cout << "The word  '" << phase << "' is NOT Palindrome." << endl;
	}

}